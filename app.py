import tkinter as tk
from tkinter import messagebox
from rooks_solver import RooksSolver

class GameBoard(tk.Frame):
    def __init__(self, parent, rows=4, columns=4, size=64, color1="white", color2="black"):
        '''config'''
        self.counter = 0
        self.rows = rows
        self.columns = columns
        self.size = size
        self.color1 = color1
        self.color2 = color2
        self.pieces = {}
        self.imagedata = '''
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAACEUlEQVRoge2Yz0tVQRTHv2e894llPn9EYGmBqxaGgeBCozb6hIp2D7RWUryN/QEt+wfERTuR2oQI/gGKGrjwbVq5tG2Bq+Al9bB83hlXgsQbr575ggXzWV3ud858z2HO4TIXiEQikUgAErrBxPNX7uR5Y2lRmr0/Szv9XoMJCf4XiAVcNtQCJl68HAOA8enKg4toIVCHWEMc4stOIBRGAVV9qNsONQ+egdO4z3erAEZzllVlZJc2yOwWyh9oJ0FD/zfUEwCA4Uol7f5lD5tpjb2+dGvr7RHTjz7EN+r1Np/WMfDVq2mhF3BwmF7xafUD49W00AswpnDVpzlr2ul+7A2TluyWT0tTe5PtRy/ACvq8WubXtPC/xFZu+yQH3GHb8WdAcM8rCgbpfuwNHeS+TxPAq2mhfsjGy5WipPY7gMSz5Mg1zPXNlYV9lif1BKTVPYI/eQBITJI9ZHpyW8jhce4SY54wLWktVC6XCz/S4h6AnpyltaTW1ru6+u4Pw5d2AvuF4jPkJw8AXY3O309ZvrQCnMXsedcK3GuWb3ALnfMS4yP4csM4Af0FhXC5UZ9AaWqmH8bMW0hJgGuaPRzwE5BPicnerH18/0Wzh6qA0tRMvzMtOwC6NfFNqInNhtaXP3y7aKCuhYyZBy95AOhyJplTpaIJspCSJi6HSU2QqgBtz5+N69BE/fd/5iKRSCQSxDHxT3kSrbzUQAAAAABJRU5ErkJggg==
        '''
        self.rook = tk.PhotoImage(data=self.imagedata)
    
        canvas_width = columns * size
        canvas_height = rows * size

        tk.Frame.__init__(self, parent)
        self.canvas = tk.Canvas(self, borderwidth=0, highlightthickness=0,
                                width=canvas_width, height=canvas_height, background="bisque")
        self.canvas.pack(side="top", fill="both", expand=True, padx=2, pady=2)
        self.buttonCalculate = tk.Button(self, text ="Calculate!", command = self.calculate)
        self.buttonCalculate.pack()
        self.buttonRefresh = tk.Button(self, text ="Refresh Board", command = self.clear)
        self.buttonRefresh.pack()
        self.canvas.bind("<Configure>", self.draw)
        self.canvas.bind("<Button-1>", self.callback)
    def callback(self, event):
        if self.counter <= 3:
            self.placepiece(str(self.counter),event.x, event.y)
        else:
            messagebox.showinfo( "Info", "Can only input 4 rooks!")
        print("clicked at", event.x, event.y)
        self.counter += 1
    
    def clear(self):
        self.canvas.delete("all")
        self.pieces.clear()
        self.counter = 0
        color = self.color2
        for row in range(self.rows):
            color = self.color1 if color == self.color2 else self.color2
            for col in range(self.columns):
                x1 = (col * self.size)
                y1 = (row * self.size)
                x2 = x1 + self.size
                y2 = y1 + self.size
                self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")
                color = self.color1 if color == self.color2 else self.color2
        self.canvas.tag_raise("piece")
        self.canvas.tag_lower("square")
    
    def calculate(self):
        rooks_array = []
        message = "Tidak Satisfiable"
        for value in self.pieces.values():
            position = value[0]*1 + value[1]*4 + 1
            rooks_array.append(position)
            print(rooks_array)
        rs = RooksSolver()
        rs.set_rooks(rooks_array)
        if rs.is_satisfied():
            message = "Satisfiable"
        messagebox.showinfo("4x4 rooks", message)


    def placepiece(self, name, row, column):
        '''Place a piece at the given row/column'''
        c = (column) // 64
        r = (row) // 64

        x0 = (c * self.size) + int(self.size/2)
        y0 = (r * self.size) + int(self.size/2)
        self.pieces[name] = (r, c)
        print("placed at", x0, y0)
        print(self.pieces)
        self.canvas.create_image(y0,x0, image=self.rook, tags=(name, "piece"), anchor="c")

    def draw(self,event):
        '''Redraw the board, possibly in response to window being resized'''
        # xsize = int((event.width-1) / self.columns)
        # ysize = int((event.height-1) / self.rows)
        # self.size = min(xsize, ysize)
        # self.canvas.delete("square")
        color = self.color2
        for row in range(self.rows):
            color = self.color1 if color == self.color2 else self.color2
            for col in range(self.columns):
                x1 = (col * self.size)
                y1 = (row * self.size)
                x2 = x1 + self.size
                y2 = y1 + self.size
                self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")
                color = self.color1 if color == self.color2 else self.color2
        for name in self.pieces:
            self.placepiece(name, self.pieces[name][0], self.pieces[name][1])
        self.canvas.tag_raise("piece")
        self.canvas.tag_lower("square")


# image comes from the silk icon set which is under a Creative Commons
# license. For more information see http://www.famfamfam.com/lab/icons/silk/
imagedata = '''
iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAACEUlEQVRoge2Yz0tVQRTHv2e894llPn9EYGmBqxaGgeBCozb6hIp2D7RWUryN/QEt+wfERTuR2oQI/gGKGrjwbVq5tG2Bq+Al9bB83hlXgsQbr575ggXzWV3ud858z2HO4TIXiEQikUgAErrBxPNX7uR5Y2lRmr0/Szv9XoMJCf4XiAVcNtQCJl68HAOA8enKg4toIVCHWEMc4stOIBRGAVV9qNsONQ+egdO4z3erAEZzllVlZJc2yOwWyh9oJ0FD/zfUEwCA4Uol7f5lD5tpjb2+dGvr7RHTjz7EN+r1Np/WMfDVq2mhF3BwmF7xafUD49W00AswpnDVpzlr2ul+7A2TluyWT0tTe5PtRy/ACvq8WubXtPC/xFZu+yQH3GHb8WdAcM8rCgbpfuwNHeS+TxPAq2mhfsjGy5WipPY7gMSz5Mg1zPXNlYV9lif1BKTVPYI/eQBITJI9ZHpyW8jhce4SY54wLWktVC6XCz/S4h6AnpyltaTW1ru6+u4Pw5d2AvuF4jPkJw8AXY3O309ZvrQCnMXsedcK3GuWb3ALnfMS4yP4csM4Af0FhXC5UZ9AaWqmH8bMW0hJgGuaPRzwE5BPicnerH18/0Wzh6qA0tRMvzMtOwC6NfFNqInNhtaXP3y7aKCuhYyZBy95AOhyJplTpaIJspCSJi6HSU2QqgBtz5+N69BE/fd/5iKRSCQSxDHxT3kSrbzUQAAAAABJRU5ErkJggg==
'''

if __name__ == "__main__":
    root = tk.Tk()
    root.title("4x4 rooks")
    root.resizable(0, 0)
    board = GameBoard(root)
    board.pack(side="top", fill="both", expand=1, padx=8, pady=8)
    # player1 = tk.PhotoImage(data=imagedata)
    # board.addpiece("player1", player1, 2,2)
    root.mainloop()