# n-Rooks SAT Solver
Before using this app, you should make sure 'minisat' is accessible from command line.
Check your minisat existance by type 'minisat --help'.

## Usage of rooks_solver.py

- Default total of Rooks: 4
```python
rs = RooksSolver()
rs.set_rooks([....])

# result: True or False [boolean]
rs.is_satisfied()
```

- Set total of Rooks
```python
...
rs.set_total_rooks(4, [1,6,11,16])

# result: True or False [boolean]
rs.is_satisfied()
```
## Run program with GUI
python ./app.py
### Variable Assignment
![alt how the variable assigned](variable-assignment.png)