from rooks_solver import RooksSolver

# Example

rs = RooksSolver()
# default position: [1,6,11,16]
print(rs.is_satisfied())

rs.set_rooks([1,2,3,4])
print(rs.is_satisfied())