import os

DEFAULT_ACTIVE_ROOKS = [1,6,11,16]


class RooksSolver:
    # Rooks SOlver for N = 4
    N = 4
    def __init__(self):
        self.kb = open("kb_%d.txt" % self.N, "r").read()
        self.rooks = self.get_rooks_clause(DEFAULT_ACTIVE_ROOKS)

    def solve(self, state):
        f_temp = open('temp.txt', 'w')
        f_temp.write(state)
        f_temp.close()

        string = os.system("minisat temp.txt result.txt")
        f_result = open('result.txt', 'r')
        result = f_result.read().split('\n')
        f_result.close()
        return result[0] == 'SAT'

    def is_satisfied(self):
        print('ROOKS', self.rooks)
        state = self.kb +'\n'+ self.rooks
        result = self.solve(state)
        return result

    def set_rooks(self, arr_of_rooks):
        self.rooks = self.get_rooks_clause(arr_of_rooks)

    def set_total_rooks(self, total_of_rooks, arr_of_rooks):
        self.N = total_of_rooks
        self.generate_kb()
        self.kb = open("kb_%d.txt" % total_of_rooks, "r").read()
        self.set_rooks(arr_of_rooks)

    def get_rooks_clause(self, active_rooks):
        clauses = ""
        for r in active_rooks:
            clauses += "%d 0\n" % r
        return clauses

    def generate_kb(self):
        n = self.N
        count_vars = n * n
        variables = [x for x in range(1, count_vars + 1)]
        clauses = {}
        count_clause = 0
        for v in variables:
            if v not in clauses:
                clauses[v] = []
            
            # rows
            # handle end of rows
            if v % n != 0:
                for r in range(n - (v % n)):
                    neighbor = v + r + 1
                    if neighbor not in clauses[v]:
                        count_clause += 1
                        clauses[v].append(neighbor)
            # cols
            # handle end of cols
            if v <= count_vars - n:
                for c in range(1, n):
                    neighbor = v + c*n
                    if neighbor not in clauses[v] and neighbor <= count_vars:
                        count_clause += 1
                        clauses[v].append(neighbor)
        
        print('CLAUSES:')
        print(clauses)

        count_clause += n # rooks position
        self.write_kb(clauses, count_clause)

    def write_kb(self, clauses, count_clause):
        n = self.N
        filename = 'kb_%d.txt' % n
        f = open(filename, 'w+')

        summary = 'p cnf %d %d\n' % (n*n, count_clause)
        f.write('c N-Rooks Formula (N = %d)\n' % n)
        f.write(summary)

        for c in clauses:
            for d in clauses[c]:
                clause_dimacs = self.disjuction_in_dimacs(-c,-d)
                f.write(clause_dimacs)

        f.write('c ROOKS position below\n')
        f.close()

    def disjuction_in_dimacs(self, a,b):
        return '%d %d 0\n' % (a,b)